﻿using back_end.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Transactions;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;// = "server=localhost;port=3306;database=tim_7_db;user=root;password=";

        public InvoiceController(IConfiguration config)
        {
            _configuration = config;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        [HttpGet]
        [Route("Invoice")]
        [Authorize(Roles = "admin")]
        public IActionResult GetAllInvoice()
        {
            List<InvoiceModel> invoices = new List<InvoiceModel>();
            string query = "SELECT a.invoice_id,a.invoice_date,COUNT(b.invoice_detail_id) AS total_course,SUM(c.course_price) AS total_price FROM invoice a\r\n" +
                "INNER JOIN invoice_detail b ON b.fk_invoice_id=a.invoice_id\r\n" +
                "INNER JOIN course c ON c.course_id=b.fk_course_id\r\n" +
                "GROUP BY a.invoice_id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            invoices.Add(new InvoiceModel
                            {
                                invoice_id = reader["invoice_id"].ToString() ?? string.Empty,
                                invoice_date = (Convert.ToDateTime(reader["invoice_date"])).ToString("dd MMMM yyyy"),
                                total_course = Convert.ToInt16(reader["total_course"]),
                                total_price = Convert.ToDecimal(reader["total_price"])
                            });
                        }
                    }
                }

                conn.Close();
            }

            return Ok(invoices);
        }

        [HttpGet]
        [Route("GetInvoice")]
        public IActionResult GetInvoice(Guid user_id)
        {
            List<InvoiceModel> invoices = new List<InvoiceModel>();
            string query = "SELECT a.invoice_id,a.invoice_date,COUNT(b.invoice_detail_id) AS total_course,SUM(c.course_price) AS total_price FROM invoice a\r\n" +
                "INNER JOIN invoice_detail b ON b.fk_invoice_id=a.invoice_id\r\n" +
                "INNER JOIN course c ON c.course_id=b.fk_course_id\r\n" +
                "WHERE a.fk_user_id=@user_id\r\n" +
                "GROUP BY a.invoice_id";

            using(MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using(MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    command.Parameters.AddWithValue("@user_id", user_id);

                    using(MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            invoices.Add(new InvoiceModel
                            {
                                invoice_id = reader["invoice_id"].ToString() ?? string.Empty,
                                invoice_date = (Convert.ToDateTime(reader["invoice_date"])).ToString("dd MMMM yyyy"),
                                total_course = Convert.ToInt16(reader["total_course"]),
                                total_price = Convert.ToDecimal(reader["total_price"])
                            });
                        }
                    }
                }

                conn.Close();
            }

            return Ok(invoices);
        }

        [HttpGet]
        [Route("GetInvoiceDetail")]
        public IActionResult GetInvoiceDetail(string invoice_id)
        {
            //List<InvoiceDetailModel> invoices_detail = new List<InvoiceDetailModel>();
            InvoiceDetailModel invoice_detail = new InvoiceDetailModel();

            string query = "SELECT b.course_name,c.type_name,d.schedule_date,b.course_price FROM invoice_detail a\r\n" +
                "INNER JOIN course b ON b.course_id=a.fk_course_id\r\n" +
                "INNER JOIN type c ON c.type_id=b.fk_type_id\r\n" +
                "INNER JOIN `schedule` d ON d.schedule_id=a.fk_schedule_id\r\n" +
                "WHERE a.fk_invoice_id=@invoice_id";

            string query2 = "SELECT a.invoice_id,a.invoice_date,COUNT(b.invoice_detail_id) AS total_course,SUM(c.course_price) AS total_price FROM invoice a\r\n" +
                "INNER JOIN invoice_detail b ON b.fk_invoice_id=a.invoice_id\r\n" +
                "INNER JOIN course c ON c.course_id=b.fk_course_id\r\n" +
                "WHERE a.invoice_id=@invoice_id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@invoice_id", invoice_id);

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CourseModel order_course = new CourseModel
                            {
                                name = reader["course_name"].ToString() ?? string.Empty,
                                type = reader["type_name"].ToString() ?? string.Empty,
                                price = Convert.ToDecimal(reader["course_price"])
                            };

                            invoice_detail.courses.Add(new CartModel
                            {
                                course = order_course,
                                course_date = Convert.ToDateTime(reader["schedule_date"]).ToString("dddd, dd MMMM yyyy")
                            });

                            //invoices_detail.Add(new InvoiceDetailModel
                            //{
                            //    course = order_course,
                            //    course_date = Convert.ToDateTime(reader["schedule_date"]).ToString("dddd, dd MMMM yyyy")
                            //});
                        }
                    }
                }

                using (MySqlCommand command2 = new MySqlCommand(query2, conn))
                {
                    command2.Parameters.AddWithValue("@invoice_id", invoice_id);

                    using (MySqlDataReader reader = command2.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            InvoiceModel invoice = new InvoiceModel
                            {
                                invoice_id = invoice_id,
                                invoice_date = Convert.ToDateTime(reader["invoice_date"]).ToString("dd MMMM yyyy"),
                                total_price = Convert.ToDecimal(reader["total_price"])
                            };

                            invoice_detail.invoice = invoice;
                        }
                    }
                }

                conn.Close();
            }

            return Ok(invoice_detail);
        }

        [HttpGet]
        [Route("GetMyClass")]
        public IActionResult GetMyClass(Guid user_id)
        {
            List<MyClassModel> myClass = new List<MyClassModel>();
            string query = "SELECT e.type_name,b.course_name,b.course_image,c.schedule_date FROM invoice_detail a\r\n" +
                "INNER JOIN course b ON b.course_id=a.fk_course_id\r\n" +
                "INNER JOIN schedule c ON c.schedule_id=a.fk_schedule_id\r\n" +
                "INNER JOIN invoice d ON d.invoice_id=a.fk_invoice_id\r\n" +
                "INNER JOIN type e ON e.type_id=b.fk_type_id\r\n" +
                "WHERE d.fk_user_id=@user_id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    command.Parameters.AddWithValue("@user_id", user_id);

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            myClass.Add(new MyClassModel
                            {
                                course_image = reader["course_image"].ToString() ?? string.Empty,
                                course_type = reader["type_name"].ToString() ?? string.Empty,
                                course_name = reader["course_name"].ToString() ?? string.Empty,
                                course_date = Convert.ToDateTime(reader["schedule_date"]).ToString("dddd, dd MMMM yyyy")
                            });
                        }
                    }
                }

                conn.Close();
            }

            return Ok(myClass);
        }

        [HttpPut]
        [Route("Checkout")]
        public IActionResult Checkout(Guid user_id, List<string> carts_id)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                MySqlTransaction transaction = conn.BeginTransaction();

                try
                {
                    string newInvoiceId = CreateInvoice(user_id);

                    foreach (string id in carts_id)
                    {
                        string query2 = "SELECT * FROM cart WHERE cart_id=@id AND isCheckout=0";

                        using (MySqlCommand command2 = new MySqlCommand(query2, conn))
                        {
                            command2.Parameters.Clear();
                            command2.Parameters.AddWithValue("@id", id);

                            string course_id = "";
                            string schedule_id = "";

                            using (MySqlDataReader reader = command2.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    course_id = reader["fk_course_id"].ToString() ?? string.Empty;
                                    schedule_id = reader["fk_schedule_id"].ToString() ?? string.Empty;
                                }

                                conn.Close();
                                conn.Open();

                                MySqlCommand command3 = new MySqlCommand();

                                command3.Connection = conn;
                                command3.Transaction = transaction;
                                command3.Parameters.Clear();

                                command3.CommandText = "INSERT INTO invoice_detail VALUES(DEFAULT,@inv_id,@course_id,@schedule_id)";
                                command3.Parameters.AddWithValue("@inv_id", newInvoiceId);
                                command3.Parameters.AddWithValue("@course_id", course_id);
                                command3.Parameters.AddWithValue("@schedule_id", schedule_id);

                                command3.ExecuteNonQuery();
                                conn.Close();
                            }

                            conn.Open();
                            MySqlCommand command4 = new MySqlCommand();

                            command4.Connection = conn;
                            command4.Transaction = transaction;
                            command4.Parameters.Clear();

                            command4.CommandText = "UPDATE cart SET isCheckout=1 WHERE cart_id=@id";
                            command4.Parameters.AddWithValue("@id", id);
                            command4.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();

                    return Ok("Checkout Success :D");

                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return StatusCode(500, ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private string CreateInvoice(Guid user_id)
        {
            string new_id = GetNewInvoiceId();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();
                    MySqlCommand command1 = new MySqlCommand();

                    command1.Connection = conn;
                    command1.Parameters.Clear();

                    command1.CommandText = "INSERT INTO invoice VALUES(@new_id,@today,@user_id,@checkout_status)";
                    command1.Parameters.AddWithValue("@new_id", new_id);
                    command1.Parameters.AddWithValue("@today", DateTime.Today.ToString("yyyy-MM-dd"));
                    command1.Parameters.AddWithValue("@user_id", user_id);
                    command1.Parameters.AddWithValue("@checkout_status", 1);

                    command1.ExecuteNonQuery();
                    conn.Close();
                }

                return new_id;
            }
            catch
            {
                return "Cannot Create Invoice !";
            }
        }

        private string GetNewInvoiceId()
        {
            string query = "SELECT COUNT(invoice_id) AS invoice_count FROM invoice";
            int invoice_count = 0;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            invoice_count = Convert.ToInt32(reader["invoice_count"]) + 1;
                        }
                    }
                }

                conn.Close();
            }

            return "DL" + invoice_count.ToString("000");
        }
    }
}
