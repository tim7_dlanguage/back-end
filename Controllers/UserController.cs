﻿using back_end.Emails;
using back_end.Emails.Template;
using back_end.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using back_end.DTOs;
using System.Security.Principal;
using Microsoft.Win32;
using Microsoft.AspNetCore.Authorization;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;
        private readonly string _connectionString;// = "server=localhost;port=3306;database=tim_7_db;user=root;password=";

        public UserController(IConfiguration config, EmailService emailService)
        {
            _configuration = config;
            _emailService = emailService;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        #region API Admin

        [HttpGet]
        [Route("User")]
        [Authorize(Roles = "admin")]
        public IActionResult GetUser()
        {
            string query = "SELECT*FROM user";
            List<UserModel> users = new List<UserModel>();

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            users.Add(new UserModel
                            {
                                Id = new Guid(reader["user_id"].ToString() ?? string.Empty),
                                Name = reader["user_name"].ToString() ?? string.Empty,
                                Email = reader["user_email"].ToString() ?? string.Empty,
                                Password = reader["user_pass"].ToString() ?? string.Empty,
                                Status = Convert.ToBoolean(reader["user_isActive"]),
                                Role = reader["user_role"].ToString() ?? string.Empty
                            });
                        }
                    }
                }

                conn.Close();
            }

            return Ok(users);
        }

        [HttpPost]
        [Route("User")]
        [Authorize(Roles = "admin")]
        public IActionResult CreateUser(UserModel user)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.Parameters.Clear();

                    Guid id = Guid.NewGuid();

                    command.CommandText = "INSERT INTO `user`(user_id,user_name,user_email,user_pass,user_isActive,user_role) VALUES(@id,@name,@email,@password,@status,@role)";
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@name", user.Name);
                    command.Parameters.AddWithValue("@email", user.Email);
                    command.Parameters.AddWithValue("@password", BCrypt.Net.BCrypt.HashPassword(user.Password));
                    command.Parameters.AddWithValue("@status", user.Status);
                    command.Parameters.AddWithValue("@role", user.Role);

                    var result = command.ExecuteNonQuery();

                    return Ok("User Added Successfully");

                }
                catch (Exception ex)
                {
                    return Problem(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        [HttpPut]
        [Route("User")]
        [Authorize(Roles = "admin")]
        public IActionResult EditUser(UserModel user)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.Parameters.Clear();

                    command.CommandText = "UPDATE `user` SET\r\n" +
                        "user_name=@name,\r\n" +
                        "user_email=@email,\r\n" +
                        "user_pass=@password,\r\n" +
                        "user_isActive=@status,\r\n" +
                        "user_role=@role\r\n" +
                        "WHERE user_id=@id";
                    command.Parameters.AddWithValue("@id", user.Id);
                    command.Parameters.AddWithValue("@name", user.Name);
                    command.Parameters.AddWithValue("@email", user.Email);
                    command.Parameters.AddWithValue("@password", BCrypt.Net.BCrypt.HashPassword(user.Password));
                    command.Parameters.AddWithValue("@status", user.Status);
                    command.Parameters.AddWithValue("@role", user.Role);

                    var result = command.ExecuteNonQuery();

                    return Ok("Edit Successfully");

                }
                catch (Exception ex)
                {
                    return Problem(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        [HttpDelete]
        [Route("User")]
        [Authorize(Roles = "admin")]
        public IActionResult DeleteUser(string user_id)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.CommandText = "DELETE FROM user WHERE user_id=@user_id";
                    command.Parameters.AddWithValue("@user_id", user_id);

                    var result = command.ExecuteNonQuery();

                    return Ok("Delete Success");
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
        }

        #endregion

        #region API User
        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterDTO register)
        {
            if (isRegistered(register.Email))
                return BadRequest("Your email has been registered");

            using(MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                MySqlTransaction transaction = conn.BeginTransaction();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    Guid id = Guid.NewGuid();
                    bool status = false;
                    string role = "user";

                    command.CommandText = "INSERT INTO `user`(user_id,user_name,user_email,user_pass,user_isActive,user_role) VALUES(@id,@name,@email,@password,@status,@role)";
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@name", register.Name);
                    command.Parameters.AddWithValue("@email", register.Email);
                    command.Parameters.AddWithValue("@password", BCrypt.Net.BCrypt.HashPassword(register.Password));
                    command.Parameters.AddWithValue("@status",  status);
                    command.Parameters.AddWithValue("@role", role);

                    UserModel user = new UserModel
                    {
                        Id = id,
                        Name = register.Name,
                        Email = register.Email,
                        Password = register.Password,
                        Status = status,
                        Role = role
                    };

                    var result = command.ExecuteNonQuery();
                    bool mailResult = await SendMailActivation(user);

                    transaction.Commit();

                    return StatusCode(201, user);
                    
                }
                catch(Exception ex)
                {
                    transaction.Rollback();

                    return Problem(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login(LoginDTO login)
        {
            UserModel currentUser = new UserModel();
            string query = "SELECT * FROM `user` WHERE user_email=@email";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    command.Parameters.AddWithValue("@email", login.Email);

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            currentUser = new UserModel
                            {
                                Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                Name = reader["user_name"].ToString() ?? string.Empty,
                                Email = reader["user_email"].ToString() ?? string.Empty,
                                Password = reader["user_pass"].ToString() ?? string.Empty,
                                Status = Convert.ToBoolean(reader["user_isActive"]),
                                Role = reader["user_role"].ToString() ?? string.Empty
                            };
                        }
                    }
                }
            }

            if (currentUser.Email == string.Empty)
                return NotFound("You aren't Registered. Please Register First!");

            if (!currentUser.Status)
                return BadRequest("Your account not activated");

            if(!BCrypt.Net.BCrypt.Verify(login.Password,currentUser.Password))
                return BadRequest("Wrong Password!");

            else
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("JwtConfig:Key").Value));

                var claims = new Claim[]
                {
                    new Claim(ClaimTypes.Sid, currentUser.Id.ToString()),
                    new Claim(ClaimTypes.Name, currentUser.Name),
                    new Claim(ClaimTypes.Email, currentUser.Email),
                    new Claim(ClaimTypes.Role, currentUser.Role)
                };

                var signingCredential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddHours(1),
                    SigningCredentials = signingCredential
                };

                var tokenHandler = new JwtSecurityTokenHandler();

                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                string token = tokenHandler.WriteToken(securityToken);

                return Ok(token);
            }
        }

        [HttpGet]
        [Route("ActivateUser")]
        public IActionResult ActivateUser(string user_email)
        {
            UserModel? user = new UserModel();
            bool result = false;

            try
            {
                using(MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using(MySqlCommand checkuser = new MySqlCommand())
                    {
                        checkuser.Connection = conn;
                        checkuser.CommandText = "SELECT * From user WHERE user_email = @user_email";
                        checkuser.Parameters.Clear();
                        checkuser.Parameters.AddWithValue("@user_email", user_email);

                        using (MySqlDataReader reader = checkuser.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                user = new UserModel
                                {
                                    Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Email = reader["user_email"].ToString() ?? string.Empty,
                                    Password = reader["user_pass"].ToString() ?? string.Empty,
                                    Status = Convert.ToBoolean(reader["user_isActive"])
                                };
                            }
                        }
                    }

                    using(MySqlCommand activateuser = new MySqlCommand())
                    {
                        activateuser.Connection = conn;
                        activateuser.Parameters.Clear();
                        activateuser.CommandText = "UPDATE user SET user_isActive=1 WHERE user_id=@user_id";
                        activateuser.Parameters.AddWithValue("@user_id", user.Id);

                        result = activateuser.ExecuteNonQuery() > 0;
                    }

                    conn.Close();
                }

                if (user == null)
                    return BadRequest("Activation Failed");

                if (user.Status == true)
                    return BadRequest("Account has been activated");

                if (result)
                    return Redirect(_configuration.GetSection("Url:front-end").Value+"/emailconfirm");

                else
                    return BadRequest("Activation Failed");
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        [Route("SendMailUser")]
        public async Task<IActionResult> SendMailUser(string mailTo)
        {
            List<string> to = new List<string>();
            to.Add(mailTo);

            string subject = "D'Language Email from Team 7";
            string body = "Hi";

            EmailModel email = new EmailModel(to, subject, body);

            bool sendMail = await _emailService.SendAsync(email, new CancellationToken());

            if (sendMail)
                return Ok("Email Send");
            else
                return StatusCode(500, "Error Sending Email");
        }

        [HttpPost]
        [Route("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                    return BadRequest("Email is empty");

                if (!isRegistered(email))
                    return BadRequest("Your email has not registered!");

                bool sendMail = await SendEmailForgetPassword(email);

                if (sendMail)
                    return Ok("Reset Password URL has been sent to your email. Please Check it");
                else
                    return StatusCode(500, "Error");
            }catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        [Route("ResetPassword")]
        public IActionResult ResetPassword(ResetPasswordDTO resetPassword)
        {
            try
            {
                if (resetPassword == null)
                    return BadRequest("No Data");

                if (resetPassword.Password != resetPassword.ConfirmPassword)
                {
                    return BadRequest("Password doesn't match");
                }

                bool reset = ResetPassword(resetPassword.Email, BCrypt.Net.BCrypt.HashPassword(resetPassword.Password));

                if (reset)
                    return Ok("Reset password success");
                else
                    return StatusCode(500, "Error");
            }catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [Route("Verify")]
        public IActionResult Verify(string token)
        {
            VerifyModel verif = new VerifyModel();

            try
            {
                var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
#pragma warning disable CS8602 // Dereference of a possibly null reference.
                verif = new VerifyModel
                {
                    Id = jwt.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid").Value,
                    Name = jwt.Claims.FirstOrDefault(c => c.Type == "unique_name").Value,
                    Email = jwt.Claims.FirstOrDefault(c => c.Type == "email").Value,
                    Role = jwt.Claims.FirstOrDefault(c => c.Type == "role").Value
                };
#pragma warning restore CS8602 // Dereference of a possibly null reference.

                return Ok(verif);

            }
            catch
            {
                return Ok(verif);
            }
        }

        #endregion

        #region Private Function
        private bool isRegistered(string email)
        {
            bool result = false;

            string query = "SELECT * FROM `user` WHERE user_email=@email";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    command.Parameters.AddWithValue("@email", email);

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        private async Task<bool> SendMailActivation(UserModel user)
        {
            if (user == null || string.IsNullOrEmpty(user.Email))
                return false;

            List<string> to = new List<string>();
            to.Add(user.Email);

            string subject = "D'Languages Account Activation";

            var param = new Dictionary<string, string>()
            {
                {"user_id",user.Id.ToString() },
                {"user_email",user.Email  }
            };

#pragma warning disable CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.
            string callback = QueryHelpers.AddQueryString(_configuration.GetSection("Url:back-end").Value+"/User/ActivateUser", param);
#pragma warning restore CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.

            //string body = "Please confirm account by clicking this <a href=\"" + callback + "\"> Link</a>";
            string body = _emailService.GetMailTemplate("EmailActivation", new ActivationModel()
            {
                Email = user.Email,
                Link = callback
            });
            //string body = "<center>" + callback + "</center>";


            EmailModel email = new EmailModel(to, subject, body);
            bool mailResult = await _emailService.SendAsync(email, new CancellationToken());

            return mailResult;
        }

        private async Task<bool> SendEmailForgetPassword(string email)
        {
            List<string> to = new List<string>();
            to.Add(email);

            string subject = "Forget Password";

            var param = new Dictionary<string, string?>
            {
                {"email",email }
            };

            string callbackUrl = QueryHelpers.AddQueryString(_configuration.GetSection("Url:front-end").Value+"/resetpass", param);
            string body = "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>";

            EmailModel mailModel = new EmailModel(to, subject, body);
            bool mainResult = await _emailService.SendAsync(mailModel, new CancellationToken());

            return mainResult;
        }

        private bool ResetPassword(string email, string pass)
        {
            bool result = false;

            string query = "UPDATE user SET user_pass=@pass WHERE user_email = @email";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();
                    command.CommandText = query;
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@pass", pass);

                    connection.Open();

                    result = command.ExecuteNonQuery() > 0;

                    connection.Close();
                }
            }

            return result;
        }

    }
    #endregion
}
