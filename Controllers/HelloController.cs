﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        [HttpGet]
        [Route("helloworld")]
        public IActionResult HelloWorld()
        {
            return Ok("Hello World!");
        }
    }
}
