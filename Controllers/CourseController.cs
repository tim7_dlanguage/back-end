﻿using back_end.Emails;
using back_end.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Reflection.Metadata.Ecma335;
using back_end.DTOs;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;// = "server=localhost;port=3306;database=tim_7_db;user=root;password=";


        public CourseController(IConfiguration config)
        {
            _configuration = config;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        [HttpGet]
        [Route("getcourseallActive")]
        public IActionResult GetCourseActive()
        {
            List<CourseModel> courses = new List<CourseModel>();
            string query = "SELECT * FROM course a " +
                   "INNER JOIN type b ON b.type_id = a.fk_type_id " +
                   "WHERE a.active = 1";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            courses.Add(new CourseModel
                            {
                                id = Convert.ToInt16(reader["course_id"]),
                                name = reader["course_name"].ToString() ?? string.Empty,
                                price = Convert.ToInt64(reader["course_price"]),
                                image = reader["course_image"].ToString() ?? string.Empty,
                                type = reader["type_name"].ToString() ?? string.Empty,
                                active = Convert.ToByte(reader["active"]),
                            });
                        }
                    }

                    conn.Close();
                }
                
            }

            return Ok(courses);
        }

        [HttpGet]
        [Route("getcourseall")]
        public IActionResult GetCourse()
        {
            List<CourseModel> courses = new List<CourseModel>();
            string query = "SELECT * FROM course a\r\n" +
                "INNER JOIN type b ON b.type_id=a.fk_type_id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            courses.Add(new CourseModel
                            {
                                id = Convert.ToInt16(reader["course_id"]),
                                name = reader["course_name"].ToString() ?? string.Empty,
                                price = Convert.ToInt64(reader["course_price"]),
                                image = reader["course_image"].ToString() ?? string.Empty,
                                type = reader["type_name"].ToString() ?? string.Empty,
                                active = Convert.ToByte(reader["active"]),
                            });
                        }
                    }

                    conn.Close();
                }

            }

            return Ok(courses);
        }

        [HttpGet]
        [Route("getcoursebyid")]
        public IActionResult GetCourseById(int id)
        {
            CourseModel course = new CourseModel();

            string query = "SELECT * FROM course a\r\n" +
                "INNER JOIN type b ON b.type_id=a.fk_type_id\r\n" +
                "WHERE course_id=@id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    comm.Parameters.AddWithValue("@id",id);
                    MySqlDataReader reader = comm.ExecuteReader();

                    if (reader.Read())
                    {
                        course = new CourseModel
                        {
                            id = Convert.ToInt16(reader["course_id"]),
                            name = reader["course_name"].ToString() ?? string.Empty,
                            price = Convert.ToInt64(reader["course_price"]),
                            image = reader["course_image"].ToString() ?? string.Empty,
                            type = reader["type_name"].ToString() ?? string.Empty
                        };
                    }

                    conn.Close();
                }

            }

            return Ok(course);
        }

        [HttpPost]
        [Route("AddCourse")]
        public IActionResult AddCourse([FromBody] CourseDTO course)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.CommandText = "INSERT INTO course (course_name, course_price, course_image, fk_type_id) VALUES (@name, @price, @image, @type_id)";
                    command.Parameters.AddWithValue("@name", course.Name);
                    command.Parameters.AddWithValue("@price", course.Price);
                    command.Parameters.AddWithValue("@image", course.Image);
                    command.Parameters.AddWithValue("@type_id", course.TypeId);

                    var result = command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }

            return Ok("Course added successfully");
        }

        [HttpPut]
        [Route("UpdateCourse")]
        public IActionResult UpdateCourse([FromBody] CourseModel updatedCourse)
        {
            if (updatedCourse == null || updatedCourse.id <= 0)
            {
                return BadRequest("Invalid request data");
            }

            string query = "UPDATE course SET course_name = @name, course_price = @price, course_image = @image, fk_type_id = @typeId WHERE course_id = @id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    comm.Parameters.AddWithValue("@id", updatedCourse.id);
                    comm.Parameters.AddWithValue("@name", updatedCourse.name);
                    comm.Parameters.AddWithValue("@price", updatedCourse.price);
                    comm.Parameters.AddWithValue("@image", updatedCourse.image);
                    comm.Parameters.AddWithValue("@typeId", updatedCourse.type);

                    int rowsAffected = comm.ExecuteNonQuery();
                    conn.Close();

                    if (rowsAffected > 0)
                    {
                        return Ok("Course updated successfully");
                    }
                    else
                    {
                        return NotFound("Course not found");
                    }
                }
            }
        }



        [HttpDelete]
        [Route("DeleteCourseById")]
        public IActionResult DeleteCourseById(int id)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.CommandText = "DELETE FROM course WHERE course_id=@id";
                    command.Parameters.AddWithValue("@id", id);

                    var result = command.ExecuteNonQuery();

                    if (result > 0)
                    {
                        return Ok("Course deleted successfully");
                    }
                    else
                    {
                        return NotFound("Course not found");
                    }
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
        }

        [HttpPatch]
        [Route("ToggleCourseStatus")]
        public IActionResult ToggleCourseStatus(int id)
        {
            string checkStatusQuery = "SELECT active FROM course WHERE course_id = @id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand checkStatusCommand = new MySqlCommand(checkStatusQuery, conn))
                {
                    conn.Open();

                    checkStatusCommand.Parameters.AddWithValue("@id", id);
                    object statusResult = checkStatusCommand.ExecuteScalar();

                    if (statusResult != null && statusResult != DBNull.Value)
                    {
                        int currentStatus = Convert.ToInt32(statusResult);
                        int newStatus = (currentStatus == 0) ? 1 : 0;

                        string updateStatusQuery = "UPDATE course SET active = @newStatus WHERE course_id = @id";

                        using (MySqlCommand updateStatusCommand = new MySqlCommand(updateStatusQuery, conn))
                        {
                            updateStatusCommand.Parameters.AddWithValue("@id", id);
                            updateStatusCommand.Parameters.AddWithValue("@newStatus", newStatus);
                            int rowsAffected = updateStatusCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                return Ok($"Course status toggled successfully. New status: {newStatus}");
                            }
                            else
                            {
                                return NotFound("Course not found");
                            }
                        }
                    }
                    else
                    {
                        return NotFound("Course not found");
                    }
                }
            }
        }


    }

}
