﻿using back_end.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using MySql.Data.MySqlClient;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public CartController(IConfiguration config)
        {
            _configuration = config;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        [Authorize]
        [HttpGet]
        [Route("GetCart")]
        public IActionResult GetCart(Guid user_id)
        {
            List<CartModel> carts = new List<CartModel>();

            string query = "SELECT a.cart_id,a.fk_user_id,b.course_name,b.course_price,b.course_image,c.type_name,d.schedule_date FROM cart a\r\n" +
                "INNER JOIN course b ON b.course_id=a.fk_course_id\r\n" +
                "INNER JOIN type c ON c.type_id=b.fk_type_id\r\n" +
                "INNER JOIN `schedule` d ON d.schedule_id=a.fk_schedule_id\r\n" +
                "WHERE fk_user_id=@user_id AND isCheckout=0";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    command.Parameters.AddWithValue("@user_id", user_id);

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CourseModel course = new CourseModel
                            {
                                name = reader["course_name"].ToString() ?? string.Empty,
                                image = reader["course_image"].ToString() ?? string.Empty,
                                price = Convert.ToInt64(reader["course_price"]),
                                type = reader["type_name"].ToString() ?? string.Empty
                            };

                            ScheduleModel schmodel = new ScheduleModel(string.Empty, Convert.ToDateTime(reader["schedule_date"]));

                            CartModel cart = new CartModel
                            {
                                Id = Convert.ToInt16(reader["cart_id"]),
                                course = course,
                                course_date = schmodel.label
                            };

                            carts.Add(cart);
                        }

                        conn.Close();
                    }
                }
            }

            return Ok(carts);
        }

        [HttpGet]
        [Route("GetPaymentMethod")]
        public IActionResult GetPaymentMethod()
        {
            List<PaymentModel> payments = new List<PaymentModel>();
            string query = "SELECT * FROM payment_method";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (Convert.ToBoolean(reader["isActive"]))
                            {
                                PaymentModel payment = new PaymentModel
                                {
                                    Id = reader["id"].ToString() ?? string.Empty,
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Image = reader["image"].ToString() ?? string.Empty,
                                    isActive = Convert.ToBoolean(reader["isActive"])
                                };

                                payments.Add(payment);
                            }
                        }

                        conn.Close();
                    }
                }
            }

            return Ok(payments);
        }

        [HttpGet]
        [Route("PaymentMethod")]
        [Authorize(Roles = "admin")]
        public IActionResult GetPaymentMethodAll()
        {
            List<PaymentModel> payments = new List<PaymentModel>();
            string query = "SELECT * FROM payment_method";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentModel payment = new PaymentModel
                            {
                                Id = reader["id"].ToString() ?? string.Empty,
                                Name = reader["name"].ToString() ?? string.Empty,
                                Image = reader["image"].ToString() ?? string.Empty,
                                isActive = Convert.ToBoolean(reader["isActive"])
                            };

                            payments.Add(payment);
                        }

                        conn.Close();
                    }
                }
            }

            return Ok(payments);
        }

        [HttpPost]
        [Route("PaymentMethod")]
        [Authorize(Roles = "admin")]
        public IActionResult AddPaymentMethod(PaymentModel payment)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.Parameters.Clear();

                    Guid id = Guid.NewGuid();

                    command.CommandText = "INSERT INTO payment_method(id,name,image,isActive) VALUES(DEFAULT,@name,@image,@isactive)";
                    command.Parameters.AddWithValue("@name", payment.Name);
                    command.Parameters.AddWithValue("@image", payment.Image);
                    command.Parameters.AddWithValue("@isactive", payment.isActive);

                    var result = command.ExecuteNonQuery();

                    return Ok("Payment Method Added Successfully");

                }
                catch (Exception ex)
                {
                    return Problem(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        [HttpPut]
        [Route("PaymentMethod")]
        [Authorize(Roles = "admin")]
        public IActionResult EditPaymentMethod(PaymentModel payment)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.Parameters.Clear();

                    command.CommandText = "UPDATE payment_method SET\r\n" +
                        "name=@name,\r\n" +
                        "image=@image,\r\n" +
                        "isActive=@isactive\r\n" +
                        "WHERE id=@id";
                    command.Parameters.AddWithValue("@id", payment.Id);
                    command.Parameters.AddWithValue("@name", payment.Name);
                    command.Parameters.AddWithValue("@image", payment.Image);
                    command.Parameters.AddWithValue("@isactive", payment.isActive);

                    var result = command.ExecuteNonQuery();

                    return Ok("Edit Successfully");

                }
                catch (Exception ex)
                {
                    return Problem(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        [HttpGet]
        [Route("GetSchedule")]
        public IActionResult GetSchedule()
        {
            List<ScheduleModel> schedules = new List<ScheduleModel>();

            string query = "SELECT * FROM schedule";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            schedules.Add(new ScheduleModel(reader["schedule_id"].ToString() ?? string.Empty, Convert.ToDateTime(reader["schedule_date"])));
                        }

                        conn.Close();
                    }
                }
            }

            return Ok(schedules);
        }

        [Authorize]
        [HttpPost]
        [Route("AddCourseToCart")]
        public IActionResult AddCourseToCart(Guid user_id, string course_id, string schedule_id)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.CommandText = "INSERT INTO cart VALUES(default,@user_id,@course_id,@schedule_id,0)";
                    command.Parameters.AddWithValue("@user_id", user_id);
                    command.Parameters.AddWithValue("@course_id", course_id);
                    command.Parameters.AddWithValue("@schedule_id", schedule_id);

                    var result = command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }

            return Ok("Item success adding to cart");
        }

        [Authorize]
        [HttpDelete]
        [Route("DeleteCartById")]
        public IActionResult DeleteCartById(string cart_id)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = conn;
                    command.CommandText = "DELETE FROM cart WHERE cart_id=@cart_id";
                    command.Parameters.AddWithValue("@cart_id", cart_id);

                    var result = command.ExecuteNonQuery();

                    return Ok("Delete Success");
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
        }
    }
}
