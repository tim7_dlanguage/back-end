﻿using System;
using back_end.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseTypeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;// = "server=localhost;port=3306;database=tim_7_db;user=root;password=";


        public CourseTypeController(IConfiguration config)
        {
            _configuration = config;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        [HttpGet]
        [Route("getcoursetypeall")]
        public IActionResult GetCourse()
        {
            List<CourseTypeModel> courses = new List<CourseTypeModel>();
            string query = "SELECT * FROM type";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            courses.Add(new CourseTypeModel
                            {
                                type_id = Convert.ToInt32(reader["type_id"]),
                                type_name = reader["type_name"].ToString() ?? string.Empty,
                                type_image = reader["type_image"].ToString() ?? string.Empty,
                                active = Convert.ToByte(reader["active"]),
                            });
                        }
                    }

                    conn.Close();
                }
            }

            return Ok(courses);
        }

        [HttpGet]
        [Route("getcoursetypeallActive")]
        public IActionResult GetCourseActive()
        {
            List<CourseTypeModel> courses = new List<CourseTypeModel>();
            string query = "SELECT * FROM type where active = 1";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    using (MySqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            courses.Add(new CourseTypeModel
                            {
                                type_id = Convert.ToInt32(reader["type_id"]),
                                type_name = reader["type_name"].ToString() ?? string.Empty,
                                type_image = reader["type_image"].ToString() ?? string.Empty,
                                active = Convert.ToByte(reader["active"]),
                            });
                        }
                    }

                    conn.Close();
                }
            }

            return Ok(courses);
        }

        [HttpGet]
        [Route("getcoursetypebyid")]
        public IActionResult GetCourseById(int id)
        {
            CourseTypeModel courseType = new CourseTypeModel();

            string query = "SELECT * FROM type WHERE type_id = @id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();
                    comm.Parameters.AddWithValue("@id", id);
                    MySqlDataReader reader = comm.ExecuteReader();

                    if (reader.Read())
                    {
                        courseType = new CourseTypeModel
                        {
                            type_name = reader["type_name"].ToString() ?? string.Empty,
                            type_image = reader["type_image"].ToString() ?? string.Empty,
                        };
                    }

                    conn.Close();
                }

            }

            return Ok(courseType);
        }

        [HttpPatch]
        [Route("ToggleCourseTypeStatus")]
        public IActionResult ToggleCourseTypeStatus(int id)
        {
            string checkStatusQuery = "SELECT active FROM type WHERE type_id = @id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand checkStatusCommand = new MySqlCommand(checkStatusQuery, conn))
                {
                    conn.Open();

                    checkStatusCommand.Parameters.AddWithValue("@id", id);
                    object statusResult = checkStatusCommand.ExecuteScalar();

                    if (statusResult != null && statusResult != DBNull.Value)
                    {
                        int currentStatus = Convert.ToInt32(statusResult);
                        int newStatus = (currentStatus == 0) ? 1 : 0;

                        string updateStatusQuery = "UPDATE type SET active = @newStatus WHERE type_id = @id";

                        using (MySqlCommand updateStatusCommand = new MySqlCommand(updateStatusQuery, conn))
                        {
                            updateStatusCommand.Parameters.AddWithValue("@id", id);
                            updateStatusCommand.Parameters.AddWithValue("@newStatus", newStatus);
                            int rowsAffected = updateStatusCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                return Ok($"Course type status toggled successfully. New status: {newStatus}");
                            }
                            else
                            {
                                return NotFound("Course type not found");
                            }
                        }
                    }
                    else
                    {
                        return NotFound("Course type not found");
                    }
                }
            }
        }


        [HttpPost]
        [Route("AddCourseType")]
        public IActionResult AddCourseType([FromBody] CourseTypeModel newCourseType)
        {
            if (newCourseType == null || string.IsNullOrEmpty(newCourseType.type_name) || string.IsNullOrEmpty(newCourseType.type_image))
            {
                return BadRequest("Invalid request data");
            }

            string query = "INSERT INTO type (type_name, type_image) VALUES (@typeName, @typeImage)";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    comm.Parameters.AddWithValue("@typeName", newCourseType.type_name);
                    comm.Parameters.AddWithValue("@typeImage", newCourseType.type_image);

                    int rowsAffected = comm.ExecuteNonQuery();
                    conn.Close();

                    if (rowsAffected > 0)
                    {
                        return Ok("Course type added successfully");
                    }
                    else
                    {
                        return StatusCode(500, "Failed to add course type");
                    }
                }
            }
        }

        [HttpPut]
        [Route("updatecoursetype")]
        public IActionResult UpdateCourseType([FromBody] CourseTypeModel updatedCourseType)
        {
            if (updatedCourseType == null || updatedCourseType.type_id <= 0)
            {
                return BadRequest("Invalid request data");
            }

            string query = "UPDATE type SET type_name = @name, type_image = @image WHERE type_id = @id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    comm.Parameters.AddWithValue("@id", updatedCourseType.type_id);
                    comm.Parameters.AddWithValue("@name", updatedCourseType.type_name);
                    comm.Parameters.AddWithValue("@image", updatedCourseType.type_image);

                    int rowsAffected = comm.ExecuteNonQuery();
                    conn.Close();

                    if (rowsAffected > 0)
                    {
                        return Ok("Course type updated successfully");
                    }
                    else
                    {
                        return NotFound("Course type not found");
                    }
                }
            }
        }



        [HttpDelete]
        [Route("deletecoursetype")]
        public IActionResult DeleteCourseType(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Invalid course type ID");
            }

            string query = "DELETE FROM type WHERE type_id = @id";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    conn.Open();

                    comm.Parameters.AddWithValue("@id", id);

                    int rowsAffected = comm.ExecuteNonQuery();
                    conn.Close();

                    if (rowsAffected > 0)
                    {
                        return Ok("Course type deleted successfully");
                    }
                    else
                    {
                        return NotFound("Course type not found");
                    }
                }
            }
        }


    }
}

