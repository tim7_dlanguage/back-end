﻿namespace back_end.Models
{
    public class InvoiceModel
    {
        public string invoice_id { get; set; } = string.Empty;
        public string invoice_date { get; set; } = string.Empty;
        public int total_course { get; set; }
        public decimal total_price { get; set; }
    }
}
