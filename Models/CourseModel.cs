﻿namespace back_end.Models
{
    public class CourseModel
    {
        public int id { get; set; }
        public string name { get; set; } = string.Empty;
        public decimal price { get; set; }
        public string image { get; set; } = string.Empty;
        public string type { get; set; } = string.Empty;
        public byte active { get; set; }
    }
}
