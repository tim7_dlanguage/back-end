﻿namespace back_end.Models
{
    public class PaymentModel
    {
        public string Id { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Image { get; set; } = string.Empty;
        public bool isActive { get; set; }
    }
}
