﻿namespace back_end.Models
{
    public class InvoiceDetailModel
    {
        //public CourseModel course { get; set; } = new CourseModel();
        //public string course_date { get; set; } = string.Empty;
        public InvoiceModel invoice { get; set; } = new InvoiceModel();
        public List<CartModel> courses { get; set; } = new List<CartModel>();
    }
}
