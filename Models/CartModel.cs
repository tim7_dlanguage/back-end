﻿namespace back_end.Models
{
    public class CartModel
    {
        public int Id { get; set; }
        public CourseModel course { get; set; } = new CourseModel();
        public string course_date { get; set; } = string.Empty;
    }
}
