﻿using System;
namespace back_end.Models
{
	public class CourseTypeModel
	{
        public int type_id { get; set; }
        public string type_name { get; set; } = string.Empty;
        public string type_image { get; set; } = string.Empty;
        public byte active { get; set; }
    }
}

