﻿namespace back_end.Models
{
    public class MyClassModel
    {
        public string course_image { get; set; } = string.Empty;
        public string course_type { get; set; } = string.Empty;
        public string course_name { get; set; } = string.Empty;
        public string course_date { get; set; } = string.Empty;
    }
}
