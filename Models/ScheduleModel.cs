﻿namespace back_end.Models
{
    public class ScheduleModel
    {
        public string id { get; set; } = string.Empty;
        public string label { get; set; } = string.Empty;

        public ScheduleModel(string _id, DateTime sch)
        {
            id = _id;
            label = sch.ToString("dddd, d MMMM yyyy");
        }
    }
}
