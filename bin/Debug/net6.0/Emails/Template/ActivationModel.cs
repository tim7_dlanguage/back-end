﻿namespace back_end.Emails.Template
{
    public class ActivationModel
    {
        public string? Email { get; set; }
        public string? Link { get; set; }
    }
}
