﻿namespace back_end.DTOs
{
    public class CourseTypeDTO
    {
        public string TypeName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public CourseTypeDTO()
        {
            // Default constructor
        }

        public CourseTypeDTO(string typeName, string description)
        {
            TypeName = typeName;
            Description = description;
        }
    }
}
