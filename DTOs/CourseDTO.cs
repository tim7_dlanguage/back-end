﻿namespace back_end.DTOs
{
    public class CourseDTO
    {
        public string Name { get; set; } = string.Empty;
        public long Price { get; set; } = 0;
        public string Image { get; set; } = string.Empty;
        public int TypeId { get; set; } = 0;

        public CourseDTO()
        {
            // Default constructor
        }

        public CourseDTO(string name, long price, string image, int typeId)
        {
            Name = name;
            Price = price;
            Image = image;
            TypeId = typeId;
        }
    }
}
